﻿<?php
  include "../model/connection.php";
  include "../model/function.php";

  $routes_count = get_count_routes($link);
  $routes_count = $routes_count[0]["count"];

  for($i = 1; $i <= $routes_count; $i++) {
    for($j = 1; $j <= $routes_count; $j++) {
      $matrix[$i][$j] = 0;
    }
  }

  for($i = 1; $i <= $routes_count; $i++) {
    for($j = 1; $j <= $routes_count; $j++) {
      if($i != $j) { 
        $result = get_intercept($link, $i, $j, "LIMIT 1");
        if(!empty($result)) {
          $matrix[$result[0]["routs1"]][$result[0]["routs2"]] = 1;
        }
      }
    }
  }
  file_put_contents(CAHCE_FILE.md5("matrix"), serialize($matrix));

  $transport_hash = array();
  $tram = json_decode(get_transport("http://map.ettu.ru/api/v2/tram/boards/?apiKey=111&order=1"), true);
  foreach ($tram["vehicles"] as $transport) {
    if(!empty($transport["ROUTE"]) || ($transport["ON_ROUTE"] != 0)) {
      $id = get_id_rout($link, $transport["ROUTE"], 1);
      $points = get_points($link, $id[0]["id_routs"], $transport["LAT"], $transport["LON"]);
      $temp = array("range" => 150, "point" => null);
      
      foreach($points as $point) {
        $alpha = get_azimut($transport["LAT"], $transport["LON"], $point["latitude"], $point["longitude"]);
        if ($temp['range'] >= abs($transport["COURSE"] - $alpha)) {
          $temp['range'] = abs($transport["COURSE"] - $alpha);
          $temp['point'] = $point;
        } elseif ($temp['range'] >= abs($transport["COURSE"] - (360 - $alpha))) {
          $temp['range'] = abs($transport["COURSE"] - (360 - $alpha));
          $temp['point'] = $point;
        }
      }
      if(empty($transport_hash[$temp['point']["id_routs"]])) $transport_hash[$temp['point']["id_routs"]] = array();
      array_push($transport_hash[$temp['point']["id_routs"]], array("type" => 1, "route" => $transport["ROUTE"], "trprt" => $transport, "point" => $temp['point']));
    }
  }

  $troll = json_decode(get_transport("http://map.ettu.ru/api/v2/troll/boards/?apiKey=111&order=1"), true);
  foreach ($troll["vehicles"] as $transport) {
    if(!empty($transport["ROUTE"]) || ($transport["ON_ROUTE"] != 0)) {
      $id = get_id_rout($link, $transport["ROUTE"], 2);
      $points = get_points($link, $id[0]["id_routs"], $transport["LAT"], $transport["LON"]);
      $temp = array("range" => 150, "point" => null);
      foreach($points as $point) {
        $alpha = get_azimut($transport["LAT"], $transport["LON"], $point["latitude"], $point["longitude"]);
        if ($temp['range'] >= abs($transport["COURSE"] - $alpha)) {
          $temp['range'] = abs($transport["COURSE"] - $alpha);
          $temp['point'] = $point;
        } elseif ($temp['range'] >= abs($transport["COURSE"] - (360 - $alpha))) {
          $temp['range'] = abs($transport["COURSE"] - (360 - $alpha));
          $temp['point'] = $point;
        }
      }
      if(empty($transport_hash[$temp['point']["id_routs"]])) $transport_hash[$temp['point']["id_routs"]] = array();
      array_push($transport_hash[$temp['point']["id_routs"]], array("type" => 2, "route" => $transport["ROUTE"], "trprt" => $transport, "point" => $temp['point']));
    }
  }

  file_put_contents(CAHCE_FILE.md5("coords_trnsport"), serialize($transport_hash));

  echo "update";
?> 