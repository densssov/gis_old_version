<?php 
  include "../model/connection.php";
  include "../model/function.php";

  echo json_encode(array("routes" => get_route($link, $_POST["id"]), "points" => get_station($link, $_POST["id"])));
?>