<?php 

	header('Content-Type: text/html; charset=utf-8');

  include "../model/connection.php";
	include "../model/function.php";
	include "../model/tree.php";

  $begin["lat"] = $_POST["latBeg"];//56.829885;
  $begin["lon"] = $_POST["lngBeg"];//60.599193;

  $end["lat"] = $_POST["latEnd"];//56.822674;
  $end["lon"] = $_POST["lngEnd"];//60.571859;

	$name_routes = get_type_routes($link);

	$range = get_h(get_urp($begin["lat"]), RANGE);
	$result_first_routes = get_points_with_route($link, $begin["lat"], $begin["lon"], $range);

	$range = get_h(get_urp($end["lat"]), RANGE);
	$result_last_routes = get_points_with_route($link, $end["lat"], $end["lon"], $range);
  while($rout_end = mysqli_fetch_assoc($result_last_routes)) {
		array_push($last_routes, $rout_end["id_routs"]);
	}
	
	$r = 2;
	while(count($last_routes) < 1) {
		$range = get_h(get_urp($begin["lat"]), RANGE * $r);
		$result_first_routes = get_points_with_route($link, $begin["lat"], $begin["lon"], $range);
		while($rout_end = mysqli_fetch_assoc($result_last_routes)) {
			array_push($last_routes, $rout_end["id_routs"]);
		}
		$r++;
	}
	
  while($rout_begin = mysqli_fetch_assoc($result_first_routes)) {
		array_push($first_routes, $rout_begin["id_routs"]);
		$intersection[$i]["transfer"] = 0;
		$intersection[$i]["oldOrderId"] = -1;
		$intersection[$i]["curOrder"] = $rout_begin["id_routs"];
		$i++;
	}
	
	$r = 2;
	while(count($first_routes) < 1) {
		$range = get_h(get_urp($begin["lat"]), RANGE * $r);
		$result_first_routes = get_points_with_route($link, $begin["lat"], $begin["lon"], $range);
		while($rout_begin = mysqli_fetch_assoc($result_first_routes)) {
			array_push($first_routes, $rout_begin["id_routs"]);
			$intersection[$i]["transfer"] = 0;
			$intersection[$i]["oldOrderId"] = -1;
			$intersection[$i]["curOrder"] = $rout_begin["id_routs"];
			$i++;
		}
		$r++;
	}
  
  // Строим матрицу маршрутов
  $matrix_routs = unserialize(file_get_contents(CAHCE_FILE.md5("matrix")));

  $i = 0;
	$transfer = 10;
  $is_break = 0;
  $current_rout = 0;
  $routs_all = array();

	// Поиск маршрутов
	while(($intersection[$i]["transfer"] <= $transfer) && (isset($intersection[$i]))) {
		if(array_search($intersection[$i]["curOrder"], $last_routes) !== false) {
			if($is_break == 0) {
				$transfer = $intersection[$i]["transfer"] + 1;
				$is_break = 1;
			} 
			$sel_order = $intersection[$i]["oldOrderId"];
			$routs_all[$current_rout] = array();
			
			array_unshift($routs_all[$current_rout], $intersection[$i]["curOrder"]);
			while($sel_order != -1) {
				array_unshift($routs_all[$current_rout], $intersection[$sel_order]["curOrder"]);
				$sel_order = $intersection[$sel_order]["oldOrderId"];	
			} 
			$current_rout++;
		} else {		

			foreach($matrix_routs[$intersection[$i]["curOrder"]] as $key => $value) {	
				if(($value != 0) && ($key != $intersection[$i]["oldOrderId"])) {
					if(array_search($key, $first_routes) === false) {		
						array_push($intersection, array("transfer" => $intersection[$i]["transfer"] + 1, "oldOrderId" => $i, "curOrder" => $key));
					}
				}
			}		
		}
		
		$i++;	
	}

	$h_begin = get_h(get_urp($begin["lat"]), RANGE);
	$h_end = get_h(get_urp($end["lat"]), RANGE);
	// остановки начала
	foreach ($routs_all as $route) {
		$points_routes = get_station_from_id($link, $begin["lat"], $h_begin["lat"], $begin["lon"], $h_begin["lon"], $route[0]);

		// Записываем начальные остановки
		$root = new Tree($points_routes);

		if(count($route) > 1) {
			for($i = 0; $i < count($route) - 1; $i++) {
				$points = array();
				$temp = $root;
				
				while($temp->get_next_point() != NULL) {
					$temp = $temp->get_next_point();
				}
				$result = get_intercept($link, $route[$i], $route[$i + 1], '');
				$points = check_intercept($result);
				
				$temp->set_next_point(new Tree($points[0]));
				$temp = $temp->get_next_point();
				$temp->set_next_point(new Tree($points[1]));
			}
		}

		$points_routes = get_station_from_id($link, $end["lat"], $h_end["lat"], $end["lon"], $h_end["lon"], $route[count($route) - 1]);

		$temp = $root;
		while($temp->get_next_point() != NULL) {
			$temp = $temp->get_next_point();
		}
		// Записали конечные остановки
		$temp->set_next_point(new Tree($points_routes));

		$temp_array = array("points" => array(), "range" => 999.0);
		
		inspect_route($link, $root, array());

		array_push($result_array, $temp_array);
	}

	$transport = unserialize(file_get_contents(CAHCE_FILE.md5("coords_trnsport")));
	// теперь накладываем тс
	$route_time = array();
	$cc = 0;
	foreach ($result_array as &$p) {
		$time = 0.0;

		if($p["range"] != 999) {
			foreach ($p["short"] as $k => &$v) {
				$v["transport"] = get_route_name($link, $v["begin"]["id_routs"]);
				$p["points"] = array_merge($p["points"], get_image_route($link, $v["begin"]["id_routs"], $v["begin"]["orders"], $v["end"]["orders"]));
				if($k == 0) {
					$urp = get_urp($begin["lat"]);
					$time += sqrt(pow(($v["begin"]["latitude"] - $begin["lat"]) * URM, 2) + pow(($v["begin"]["longitude"] - $begin["lon"]) *  $urp, 2)) / VELOSITY_PLE;
				} else {
					$urp = get_urp($begin["lat"]);
					$time += sqrt(pow(($p["short"][$k]["begin"]["latitude"] - $p["short"][$k - 1]["end"]["latitude"]) * URM, 2) + pow(($p["short"][$k]["begin"]["longitude"] - $p["short"][$k - 1]["end"]["longitude"]) *  $urp, 2)) / VELOSITY_PLE;
				}
				$temp_time = array();
				if(empty($transport[$v["begin"]["id_routs"]])) {
					$p["time"] = "not";
					$time += 100;
					continue;
				} 
				foreach($transport[$v["begin"]["id_routs"]] as $tr) {
					$tr_time = 0.0;
					if($tr["point"]["point_name"] != "") {
						$full_routes = get_full_route($link, $tr["point"]["id_routs"]);
						$array_range = get_station_length($link, $tr["point"]["id_routs"]);

						$key = search_for_id($tr["point"]["id_points"], $full_routes);
						while(1) {
							$tr_time += $array_range[$full_routes[$key]["id_points"]][$full_routes[$key + 1]["id_points"]];
							if($v["begin"]["id_points"] == $full_routes[$key]["id_points"]) break;
							if($key == count($full_routes) - 1) {
								$key = 0;
							} else {
								$key++;
							}
						}
						array_push($temp_time, ($tr_time / (($tr["trprt"]["VELOCITY"]  + VELOSITY_TS) / 2)));
					} else {
						$r = get_route($link, $tr["point"]["id_routs"]);
						$i = search_for_id($tr["point"]["id_points"], $r);
						while(1) {
							$urp = get_urp($begin["lat"]);
							$tr_time += sqrt(pow(($r[$i + 1]["latitude"] - $r[$i]["latitude"]) * URM, 2) + pow(($r[$i + 1]["longitude"] - $r[$i]["longitude"]) *  $urp, 2));
							if($r[$i]["point_name"] != "") break;
							if($i == count($r) - 1) {
								$i = 0;
							} else {
								$i++;
							}
						}

						$array_range = get_station_length($link, $tr["point"]["id_routs"]);
						$key = search_for_id($r[$i]["id_points"], $r);

						while(1) {
							$tr_time += $array_range[$r[$key]["id_points"]][$r[$key + 1]["id_points"]];
							if($v["begin"]["id_points"] == $r[$key]["id_points"]) break;
							if($key == count($r) - 1) {
								$key = 0;
							} else {
								$key++;
							}
						}

						array_push($temp_time, ($tr_time / (($tr["trprt"]["VELOCITY"]  + VELOSITY_TS) / 2)));
					}
				}
				// ищем минимум сравниваем с time и запись
				asort($temp_time);
				$is_tr = true;
				$temp = 0;
				foreach($temp_time as $kk => $vv) {
					if ($time <= $vv) {
						$time += $vv - $time;
						$v["time"] = $vv;
						$is_tr = false;
						$temp = $kk;
						break;
					}
				}
				
				if($is_tr) {
					$rr = get_route($link, ($tr["point"]["id_routs"]));
					$time += ($temp_time[$temp] + $rr[0]["duration_plan"]) / VELOSITY_TS;
					$v["time"] = ($temp_time[$temp] + $rr[0]["duration_plan"]) / VELOSITY_TS;
				}
				$time += $v["range"]  / VELOSITY_TS;
			}
		} else {
			$time = 100;
		}
		
		$p["time"] = $time;

		array_push($route_time, $p["time"]);
	}

	$sort_array = array();
	for($i = 0; $i < count($route_time); $i++) {
		array_push($sort_array, $i);
	}
	array_multisort($route_time, SORT_ASC, $sort_array);

	$result_print = array();
	for($i = 0; $i < 5; $i++) {
		array_push($result_print, $result_array[$sort_array[$i]]);
	}
	echo json_encode($result_print);
?>