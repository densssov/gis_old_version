<?php
  include "../model/connection.php";
  include "../model/function.php";

  header('Content-Type: text/event-stream');

  $tram = json_decode(get_transport("http://map.ettu.ru/api/v2/tram/boards/?apiKey=111&order=1"), true);
  $troll = json_decode(get_transport("http://map.ettu.ru/api/v2/troll/boards/?apiKey=111&order=1"), true);

  echo "data: ".json_encode(array("tram" => $tram["vehicles"], "troll" => $troll["vehicles"]));
  echo "\n\n";   
?>