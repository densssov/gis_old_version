<?php header('Content-Type: text/html; charset=utf-8'); ?>
<html>
  <head>
    <link rel = "stylesheet" type = "text/css" href = "css/index.css"> 	
    <link rel="stylesheet" type="text/css" href="css/jquery.svg.css"> 
    <script src="js/jquery.js" type="text/javascript"></script>
    <script src="js/map.js" type="text/javascript"></script>
    <script src="js/coords.js" type="text/javascript"></script>
    <script src="https://maps.api.2gis.ru/2.0/loader.js?pkg=full"></script>
    <script src="js/jquery.svg.js" type="text/javascript" ></script>
  </head>
</html
<?php include "/view/index.html" ?>