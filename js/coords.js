// Получаем маршрут
function get_routes(id, color_id) {
  if ($("#"+id).prop("checked")) {
    $.ajax({
      type: "POST",
      data: { id: id},
      url: "controller/data.php",
      success: function(data) {
        var obj = JSON.parse(data)
        console.log(obj);
        print_route(id, obj["routes"], color_id);
        print_markers(id, obj["points"], color_id);
      }
    });
  } else {
    del_route(id);
  }  
}