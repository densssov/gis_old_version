var flag_vipad = '';
var flag = 0;
var routes = [];
var markers = [];
var map;
var color = ["red", "green", "blue", "black"];
var online = false;
var evtSource;
var types = [];
var transport = [];
var labels = ['img/a.png', 'img/b.png'];
var labelIndex = 0;
var arrPoint = [];
var constract;
var markPark = [];
var marshruts = [];

$( document ).ready(function() {
  DG.then(function () {
    map = DG.map('map', {
        center: [56.8359209, 60.6114863], // 56.835715, 60.608976 -> 135 sm
        zoom: 13
    });
    map.on("click", function(ev) {
      if(labelIndex < 2 && flag == 1) addMarker(ev.latlng)
    });
  });

  $("div[id*='vipad-']").hide();  
  document.getElementById("btn").addEventListener("click", function() {
    var menu = document.getElementById("menu");
    if(menu.classList.contains("in")) {
      menu.classList.remove("in");
    } else {
      menu.classList.add("in");
    }
  }, false);
  
  get_all_routes();
  types['Трамвай'] = "tram";
  types['Троллейбус'] = "troll";
});

var timerId = setInterval(function() {
  $.ajax({
    type: "POST",
    url: "controller/daemon.php",
    success: function(data) {
      //console.log(data);
    }
  });
}, 30000);

function addMarker(location) {
  DG.then(function () {
    var myIcon = DG.icon({
      iconUrl: labels[labelIndex],
      iconSize: [32, 32]
    });

    arrPoint[labelIndex] = DG.marker(location, {icon: myIcon, draggable: true}).addTo(map);
    labelIndex++;
  });
}

function toggle(objName) {
	if(flag_vipad != '#vipad-2') {
		if(objName == '#vipad-2') flag = 1;
		flag_vipad = objName;
	} else {
		flag = 0;
		flag_vipad = "";
	}
	
  var obj = $(objName),
    blocks = $("div[id*='vipad-']");
	
    if (obj.css("display") != "none") {
      obj.animate({ height: 'hide' }, 200);
    } else {
      var visibleBlocks = $("div[id*='vipad-']:visible");
      if (visibleBlocks.length < 1) {
        obj.animate({ height: 'show' }, 400);
      } else {
        $(visibleBlocks).animate({ height: 'hide' }, 200, function() {
          obj.animate({ height: 'show' }, 400);
        });            
      }  
    }
	
	for(var i = 1; i < 8; i++) {
		if($("#"+i).prop("checked")) { 
			$("#"+i).attr("checked", false);
		}
	}
	//clears();
}	

function marhToggle(objName) {
	otherPath(objName);
  var obj = $("#path-"+objName),
    blocks = $("div[id*='path-']");
  
  if (obj.css("display") != "none") {
    obj.animate({ height: 'hide' }, 200);
  } else {
    var visibleBlocks = $("div[id*='path-']:visible");
    if (visibleBlocks.length < 1) {
      obj.animate({ height: 'show' }, 400);
    } else {
      $(visibleBlocks).animate({ height: 'hide' }, 200, function() {
        obj.animate({ height: 'show' }, 400);
      });            
    }  
  }
}	

function get_array_coords(arr) {
  var coordinates = [];
  arr.forEach(function(point){
    coordinates.push([point["latitude"], point["longitude"]]);
  });
  return coordinates;
}

function print_route(id, obj, color_id) {
  if((routes.filter(r => r != undefined).length == 0)) {
    evtSource = new EventSource("./controller/transport.php");
    evtSource.onmessage = function(e) {
      var obj = JSON.parse(e.data);
      for (var rout in routes) {
        if (transport[rout] != []) del_transport(rout);
        transport[rout] = [];
        obj[types[$("#"+rout).data().type]].filter(t => t["ROUTE"] == [$("#"+rout).data().name]).forEach(function(t) {
          DG.then(function() {
            myDivIcon = DG.divIcon({
              iconSize: [70, 20],
              className: 'my-div-icon',
              html: '<svg style="transform: rotate('+t["COURSE"]+'deg);" width="40px" height="40px" viewBox="0 0 400 400"xmlns="http://www.w3.org/2000/svg"><path d="M 150 200 L 0 0 L 390 200 L 0 390 z" fill="orange" stroke="black" stroke-width="30"/></svg>'
            });
            transport[id].push(DG.marker([t["LAT"], t["LON"]], {
              icon: myDivIcon
            }).addTo(map));
          });
        });
      }
    };
  }

  // routes
  DG.then(function () {
    routes[id] = DG.polyline(get_array_coords(obj), {
      color: color[color_id]
    }).addTo(map);
    map.fitBounds(routes[id].getBounds());
  });
}

function print_markers(id, obj, type) {
  DG.then(function () {
    if(type == 0) {
      var myIcon = DG.icon({
        iconUrl: 'img/train.png',
        iconSize: [30, 30]
      });
    } else {
      var myIcon = DG.icon({
        iconUrl: 'img/bus.png',
        iconSize: [30, 30]
      });
    }

    markers[id] = [];
    obj.forEach(function(point) {
      markers[id].push(DG.marker([(point["latitude"]), (point["longitude"])], {icon: myIcon})
        .addTo(map)
        .bindLabel(point["point_name"])
      );
    });
  });
}

function del_transport(id) {
  if (transport[id] != undefined) {
    transport[id].forEach(function(point) {
      point.remove();
    });
  }
}

function del_route(id) {
  del_marker(id);
  del_transport(id);
  routes[id].remove();
  delete routes[id];
  if(routes.filter(r => r != undefined).length == 0) {
    evtSource.close();
  }
}

function del_marker(id) {
  markers[id].forEach(function(point) {
    point.remove();
  });
}

function get_all_routes() {
  $.ajax({
    type: "POST",
    url: "controller/routes.php",
    success: function(data) {
      var obj = JSON.parse(data);
      var color_id = 2;
      console.log(obj);
      obj.forEach(function(route) {
        if (route.type_name == "Трамвай") {
          color_id = 0;
        } else { 
          color_id = 2;
        }
        $(".tbl_div").append("<tr><td><input type='checkbox' name='route_"+route.num_rout+"' id="+route.id_routs+" data-name="+route.num_rout+" data-type="+route.type_name+" onclick='get_routes("+route.id_routs+", "+color_id+");'/><label for="+route.id_routs+"><span></span>"+route.type_name+" № "+route.num_rout+"</br>("+route.name+")</label></td></tr>");
      });
    }
  });
}

function start() {
  console.log(arrPoint[0].getLatLng());
  console.log(arrPoint[1].getLatLng());
  if(arrPoint.length == 2) {

    $.ajax({
			type: "POST",
			data: {
				latBeg: arrPoint[0].getLatLng()["lat"],
				lngBeg: arrPoint[0].getLatLng()["lng"],
				latEnd: arrPoint[1].getLatLng()["lat"],
				lngEnd: arrPoint[1].getLatLng()["lng"]
			},
			url: "controller/find_path.php",
			success: function(data) {
        
        var obj = JSON.parse(data);
        console.log(obj);
        var print = get_array_coords(obj[0]["points"]);
        print.push([arrPoint[1].getLatLng()["lat"], arrPoint[1].getLatLng()["lng"]]);
        print.unshift([arrPoint[0].getLatLng()["lat"], arrPoint[0].getLatLng()["lng"]]);
        // routes
        DG.then(function () {
          constract = DG.polyline(print, {
            color: "black"
          }).addTo(map);
          map.fitBounds(constract.getBounds());
        });

        var path = obj[0]["short"];
        console.log(path);
        var i = 0;
        var img = 1;
        while(i <= path.length - 1) {
          var myIcon = DG.icon({
            iconUrl: "img/"+img+".png",
            iconSize: [30, 30]
          });
          markPark.push(
            DG.marker([path[i]["begin"]["latitude"], path[i]["begin"]["longitude"]], {icon: myIcon}).addTo(map)
          );

          markPark.push(
            DG.marker([path[i]["end"]["latitude"], path[i]["end"]["longitude"]], {icon: myIcon}).addTo(map)
          );
          img++;
          i++;
        }
        var text;
        var count = obj.length;
        var idDiv = document.getElementById("vipad-3");
				idDiv.innerHTML = "";
        var common = "";
        for(var j = 0; j < count; j++) {
          if (obj[j]["range"] == 999) continue;
          var house = "";
          text = "<div class='texts' style='text-align:left; margin-left:5px;'>";
          if(Number((obj[j]["time"])) >= 1.0) house = parseInt((obj[j]["time"]), 10)+" ч. "; 
          text += "<p class='viewInf' onclick="+'"'+"marhToggle('"+j+"')"+'"'+";><span class='blues' >Время в пути</br></span><span style='font-size:16pt;'>"+house+parseInt(((obj[j]["time"] - parseInt(obj[j]["time"])) * 60), 10)+" мин.";
          text += "</span></p></div>";
          text += "<div id='path-"+j+"'>";
					text += "<div><p style='text-align: center; color:rgba(255,255,255,0.3);'>маршрут</p></div>";
          text += "<table style='font-size:14pt;'>";
          // Короткий вариант
          path = obj[j]["short"];

          var rout = 1;
          i = 0;
          console.log(j);
          while(i <= path.length - 1) {
            text += "<tr><td style='border-right:1px solid rgba(255,255,255,0.3);'><img width='30px' height='30px' src='img/"+rout+".png' /></td>";
            text += "<td><span class='blues'>"+path[i]["transport"][0]["type_name"]+" № "+path[i]["transport"][0]["num_rout"]+"</span></br>"+"<span class='texts'>ост. "+path[i]["begin"]["point_name"]+"</br> через "+parseInt(path[i]["time"] * 60)+" минут(ты)</br>";
            text += "до ост. "+path[i]["end"]["point_name"]+"</span></td></tr>";
            rout++;
            i++;
          }
          text += "</table></div>";
          common += text;
        }
        idDiv.innerHTML += common;
				marshruts = obj;
				$("div[id*='path-']").hide();
        marhToggle("0");
        ///
        var element = $("#vipad-3"),
					blocks = $("div[id*='vipad-']");
				if (element.css("display") != "none") {
					element.animate({ height: 'hide' }, 200);
				} else {
					var visibleBlocks = $("div[id*='vipad-']:visible");
					if (visibleBlocks.length < 1) {
						element.animate({ height: 'show' }, 400);
					} else {
						$(visibleBlocks).animate({ height: 'hide' }, 200, function() {
							element.animate({ height: 'show' }, 400);
						});            
					}  
				}
				flag_vipad = "#vipad-3";
      }
    });	
  } else {
    console.log("Точки не выбраны");
  }
}

function otherPath(numRout) {
  if (constract != undefined) {
    var print = get_array_coords(marshruts[numRout]["points"]);
    constract.remove();
    for(var i = 0; i < markPark.length; i++) {
      markPark[i].remove();
    }
    print.push([arrPoint[1].getLatLng()["lat"], arrPoint[1].getLatLng()["lng"]]);
    print.unshift([arrPoint[0].getLatLng()["lat"], arrPoint[0].getLatLng()["lng"]]);
    constract = DG.polyline(print, {
      color: "black"
    }).addTo(map);
    map.fitBounds(constract.getBounds());

    var path = marshruts[numRout]["short"];
    var i = 0;
    var img = 1;
    while(i <= path.length - 1) {
      var myIcon = DG.icon({
        iconUrl: "img/"+img+".png",
        iconSize: [30, 30]
      });
      markPark.push(
        DG.marker([path[i]["begin"]["latitude"], path[i]["begin"]["longitude"]], {icon: myIcon}).addTo(map)
      );

      markPark.push(
        DG.marker([path[i]["end"]["latitude"], path[i]["end"]["longitude"]], {icon: myIcon}).addTo(map)
      );
      img++;
      i++;
    }
  }
}

function clears() {
	constract.remove();
	for(var i = 0; i < markPark.length; i++) {
		markPark[i].remove();
	}
	arrPoint[0].remove();
	arrPoint[1].remove();

	labelIndex = 0;
	arrPoint = [];
}