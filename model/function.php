<?php
  define("CAHCE_FILE", "../cache/");
  define("URM", 111.19426645);
  define("PI", 3.14159265);
  define("RANGE", 0.15);
  define("VELOSITY_PLE", 5);
  define("VELOSITY_TS", 10);

  $i = 0;
  $last_routes = array();
  $intersection = array();
  $first_routes = array();
  $result_array = array();
  $temp_array = array();

  function get_urp($lat) {
    return cos(($lat * PI) / 180) * URM;
  }

  function get_h($urp, $r) {
		$h["lat"] = $r / URM;
		$h["lon"] = $r / $urp;
		return $h;
	}

  function get_data($query, $hash, $link) {
    $routes = array();
    $md5 = md5($hash);
    if(file_exists(CAHCE_FILE.$md5)) {
      $routes = unserialize(file_get_contents(CAHCE_FILE.$md5));
    } else {
      $result = mysqli_query($link, $query);
      while($data = mysqli_fetch_assoc($result)) {
        array_push($routes, $data);
      }
      file_put_contents(CAHCE_FILE.$md5, serialize($routes));
    }
    return $routes;
  }

  function get_id_rout($link, $num, $type) {
    $query = "
      SELECT id_routs 
      FROM routes
      WHERE num_rout = ".$num." AND
      id_type = ".$type."
    ";
    $hash = "routes_".$type."_".$num;
    return get_data($query, $hash, $link);
  }

  function get_points($link, $id, $lat, $lon) {
    
    $query = "
      SELECT DISTINCT p.point_name, t.id_routs, p.latitude, t.station_status,
      p.longitude, t.orders, p.id_points, t.direction,
      SQRT(POW((".$lat." - p.latitude) * ".URM.", 2) + POW((".$lon." - p.longitude) * ".get_urp($lat).", 2)) as result
      FROM points p, transport_station t
      WHERE p.id_points = t.id_points 
      AND t.id_routs = ".$id."
      ORDER BY result ASC 
      LIMIT 4
    ";
    $points = array();
    $result = mysqli_query($link, $query);
    while($data = mysqli_fetch_assoc($result)) {
      array_push($points, $data);
    }
    return $points;
  }

  function get_intercept($link, $id_start, $id_end, $limit) {
    $query = "
        SELECT DISTINCT t1.id_routs as routs1, p1.point_name as name1, p1.latitude as latitude1, p1.longitude as longitude1, t1.orders as orders1, t1.id_points as id_points1, t1.direction as direction1,
        t2.id_routs as routs2, p2.point_name as name2, p2.latitude as latitude2, p2.longitude as longitude2, t2.orders as orders2, t2.id_points as id_points2, t2.direction as direction2
        FROM points p1, transport_station t1, points p2, transport_station t2
        WHERE p1.id_points = t1.id_points
        AND p2.id_points = t2.id_points
        AND ABS(p1.latitude - p2.latitude) <= ".RANGE / URM."  
        AND ABS(p1.longitude - p2.longitude) <= (".RANGE." / (cos((p1.longitude * ".PI.") / 180) * ".URM."))
        AND t1.station_status > 0
        AND t2.station_status > 0
        AND t1.id_routs = ".$id_start."
        AND t2.id_routs = ".$id_end."
        ".$limit." 
    ";
    $hash = "matrix_".$id_start."_".$id_end."_".$limit;
    return get_data($query, $hash, $link);
  }


  function search_for_id($id, $array) {
    if(!empty($array)) {
      foreach ($array as $key => $val) {
          if ($val['id_points'] === $id) {
            return $key;
          }
      }
    }
    return -1;
  }

  function check_intercept($points) {
    $result = array(0 => array(), 1 => array());
    foreach ($points as $point) {
      //echo "Получил ".(search_for_id($point["id_points1"], $result[0]) == null)."</br>";
      if(search_for_id($point["id_points1"], $result[0]) == -1) {
        array_push($result[0], array("id_routs" => $point["routs1"], "name" => $point["name1"], "latitude" => $point["latitude1"], "longitude" => $point["longitude1"], "orders" => $point["orders1"], "id_points" => $point["id_points1"], "direction" => $point["direction1"]));
      }
      if(search_for_id($point["id_points2"], $result[1]) == -1) {
        array_push($result[1], array("id_routs" => $point["routs2"], "name" => $point["name2"], "latitude" => $point["latitude2"], "longitude" => $point["longitude2"], "orders" => $point["orders2"], "id_points" => $point["id_points2"], "direction" => $point["direction2"]));
      }
    }
    return $result;
  }

  function get_count_routes($link) {
    $query = "
        SELECT count(*) as count
        FROM routes
    ";
    $hash = "all_routes";
    return get_data($query, $hash, $link);
  }

  function get_route($link, $id) {
    $query = "
        SELECT p.id_points, p.point_name, p.latitude, p.longitude, r.duration_plan
        FROM points p, transport_station t, routes r
        WHERE p.id_points = t.id_points AND t.id_routs = ".$id." and t.id_routs = r.id_routs
        ORDER BY t.orders
    ";
    $hash = "route_".$id;
    return get_data($query, $hash, $link);
  }

  function get_image_route($link, $id, $begin, $end) {
    $query = "
        SELECT p.latitude, p.longitude
        FROM points p, transport_station t
        WHERE p.id_points = t.id_points AND t.id_routs = ".$id." 
        AND t.orders >= ".$begin." 
        AND t.orders <= ".$end."
        ORDER BY t.orders
    ";
    $hash = "image_route".$id.$begin.$end;
    return get_data($query, $hash, $link);
  }

  function get_full_route($link, $id) {
    $query = "
        SELECT p.id_points, p.point_name, p.latitude, p.longitude, t.orders, t.station_status, t.id_routs, t.direction
        FROM points p, transport_station t 
        WHERE p.id_points = t.id_points 
          AND t.id_routs = ".$id." 
          AND t.station_status > 0
        ORDER BY t.orders
    ";
    $hash = "full_route_".$id;
    return get_data($query, $hash, $link);
  }

  function get_station($link, $id) {
    $query = "
      SELECT DISTINCT t.id_routs, p.point_name, p.latitude, p.longitude 
      FROM transport_station t, points p 
      WHERE t.id_points = p.id_points and point_name <> '' and t.id_routs = ".$id."
    ";
    $hash = "station_route_".$id;
    return get_data($query, $hash, $link);
  }

  function get_route_name($link, $id) {
    $query = "
      SELECT r.num_rout, t.type_name 
      FROM routes r, type_transport t  
      WHERE t.id_type = r.id_type AND r.id_routs = ".$id;
    $hash = "route_name".$id;
    return get_data($query, $hash, $link);
  }

  function get_type_routes($link) {
    $query = "
      SELECT r.id_routs, r.num_rout, r.name, t.type_name
      FROM routes r, type_transport t
      WHERE t.id_type = r.id_type
      ORDER BY r.id_type
    ";
    $hash = "type_routes";
    return get_data($query, $hash, $link);
  }

  function get_type_route_id($link) {
    $query = "
      SELECT r.id_routs, r.num_rout, r.name, t.type_name
      FROM routes r, type_transport t
      WHERE t.id_type = r.id_type
      ORDER BY r.id_type
    ";
    $hash = "type_routes";
    return get_data($query, $hash, $link);
  }
  
  function get_transport($url) {
    $result = file_get_contents($url, false, stream_context_create(array(
      'http' => array(
        'method'  => 'GET',
        'header'  => 'Content-type: application/x-www-form-urlencoded'
      )
    )));
    return $result;
  }

  function get_azimut($lat1, $lon1, $lat2, $lon2) {
    $lambda = $lon2 - $lon1;
    $y = sin(deg2rad($lambda)) * cos(deg2rad($lat1));
    $x = cos(deg2rad($lat1)) * sin(deg2rad($lat2)) - sin(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($lambda));
    $tetta = atan2($y, $x);
    $tetta = rad2deg($tetta);
    $tetta += 90;
    if($tetta > 360) {
      $tetta -= 360;
    } else if($tetta < 0) {
      $tetta += 360;
    }
    return $tetta;
  }

  function get_points_with_route($link, $lat, $lon, $range) {
    $query = "
      SELECT DISTINCT t.id_routs FROM points p, transport_station t
			WHERE p.id_points = t.id_points
			AND p.latitude > ".($lat - $range["lat"])." 	 
			AND p.latitude < ".($lat + $range["lat"])."
			AND p.longitude > ".($lon - $range["lon"])." 	 
			AND p.longitude < ".($lon + $range["lon"])."
      AND t.station_status > 0
    ";
    $result = mysqli_query($link, $query);
    return $result;
  }
  

  function get_station_from_id($link, $lat, $hlat, $lon, $hlon, $id_rout) {
    $query = "
      SELECT DISTINCT p.point_name, t.id_routs, p.latitude, p.longitude, t.orders, p.id_points, t.direction
      FROM points p, transport_station t
      WHERE p.id_points = t.id_points
      AND t.id_routs = ".$id_rout."
      AND p.latitude > ".($lat - $hlat)." 	 
      AND p.latitude < ".($lat + $hlat)."
      AND p.longitude > ".($lon - $hlon)." 	 
      AND p.longitude < ".($lon + $hlon)."
      AND t.station_status > 0
    ";
    $result = mysqli_query($link, $query);
    
    $arr = array();
    while($name_point = mysqli_fetch_assoc($result)) {
      array_push($arr, $name_point);
    }		
		return $arr;
  }
  
  function get_station_length($link, $id) {
    $query = "
      SELECT * FROM lengths 
      WHERE point_1 IN (SELECT id_points FROM transport_station WHERE id_routs = ".$id.") 
      AND point_2 IN (SELECT id_points FROM transport_station WHERE id_routs = ".$id.") 
    ";
    $hash = "station_length_".$id;
    $lengths = get_data($query, $hash, $link);
    $result = array();
    foreach ($lengths as $l) {
      $result[$l["point_1"]][$l["point_2"]] = $l["length"];
    }
    return $result;
  }

  // Функция разбора дерева OverTimeHard при 0-1 пересадках 1.16 секунды, при 3-х пересадках 4-4.5 секунд warning not edit
  function inspect_route($link, $root, $arr) {
    global $temp_array;
    global $result_array;

    foreach($root->get_poin_routes() as $point) {
      array_push($arr, $point);
      if($root->get_next_point() != NULL) {
        if(count($arr) > 1) {
          if($arr[count($arr) - 1]["id_routs"] != $arr[count($arr) - 2]["id_routs"]) {
            $urp = get_urp($arr[count($arr) - 2]["latitude"]);
            $time = sqrt(pow(($arr[count($arr) - 2]["latitude"] - $arr[count($arr) - 1]["latitude"]) * URM, 2) + pow(($arr[count($arr) - 2]["longitude"] - $arr[count($arr) - 1]["longitude"]) *  $urp, 2));
            if($time <= RANGE) {
							inspect_route($link, $root->get_next_point(), $arr);	
						}
          } else {
            if(($arr[count($arr) - 2]["direction"] == $arr[count($arr) - 1]["direction"]) && ($arr[count($arr) - 2]["orders"] < $arr[count($arr) - 1]["orders"])) {
							inspect_route($link, $root->get_next_point(), $arr);	
						}
          }
        } else {
          inspect_route($link, $root->get_next_point(), $arr);
        }
      } else {
        if(($arr[count($arr) - 2]["direction"] == $arr[count($arr) - 1]["direction"]) && ($arr[count($arr) - 2]["orders"] < $arr[count($arr) - 1]["orders"])) {
          $full_routes = array();
          $path = array("short" => array(), "points" => array(), "range" => 0.0);
          
          for($i = 0; $i < count($arr) - 1; $i++) {
            $short_array = array("begin" => array(), "end" => array(), "range" => 0.0);
            if($arr[$i]["id_routs"] == $arr[$i + 1]["id_routs"]) {
              $full_routes = get_full_route($link, $arr[$i]["id_routs"]);
              $array_range = get_station_length($link, $arr[$i]["id_routs"]);
              $key = search_for_id($arr[$i]["id_points"], $full_routes);

              $short_array["begin"] = $full_routes[$key];
              while(1) {
                $short_array["range"] += $array_range[$full_routes[$key]["id_points"]][$full_routes[$key + 1]["id_points"]];
                $path["range"] += $array_range[$full_routes[$key]["id_points"]][$full_routes[$key + 1]["id_points"]];
                // array_push($path["points"], $full_routes[$key]);
                if($arr[$i + 1]["id_points"] == $full_routes[$key]["id_points"]) {
                  $short_array["end"] = $full_routes[$key];
                  break;
                }
                
                if($key == count($full_routes) - 1) {
                  $key = 0;
                } else {
                  $key++;
                }
              }
              array_push($path["short"], $short_array);
            } else {
              $urp = get_urp($arr[$i]["latitude"]);
              $dist = sqrt(pow(($arr[$i + 1]["latitude"] - $arr[$i]["latitude"]) * URM, 2) + pow(($arr[$i + 1]["longitude"] - $arr[$i]["longitude"]) *  $urp, 2));
              $path["range"] += $dist;
            }
            if(($temp_array["range"] < $path["range"]) && (!empty($temp_array["points"]))) {
              $path["range"] += 1000.0;
              break;
            }
          }
          
          if((empty($temp_array["points"])) || ($temp_array["range"] >= $path["range"])) {
            $temp_array = $path;
          }
        }
      }
      array_pop($arr);
    }
  }
?>