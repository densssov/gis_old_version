<?php 
  class Tree {
    private $point_routes;
    private $next_point;

    
    function __construct($routes) {
      $this->point_routes = $routes;
			$this->next_point = NULL;
    }
    
    public function set_next_point($point) {
      $this->next_point = $point;
    }

    public function get_next_point() {
      return $this->next_point;
    }

    public function get_route() {
      return $this->point_routes;
    }

    public function get_poin_routes() {
      return $this->point_routes;
    }

    public function get_count_point() {
			return count($this->point_routes);
		}
  }
?>